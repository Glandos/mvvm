import Vue from 'vue'
import App from './App.vue'
import { ActionMap, knownSelector, parseActions, SELECTOR_TO_ACTION_TYPE } from './model/actions'

import VueSvgGauge from 'vue-svg-gauge'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
Vue.use(VueSvgGauge as any)

// Exclude non-vue.php pages
let realWindow = window

try {
  realWindow = unsafeWindow
} catch { }

try {
  // Trigger error if any, and defined shims
  if (GM.info && 1 > 2) {
    console.warn('Wait, what?')
  }
} catch {
  (realWindow as (typeof realWindow & { GM: Pick<typeof GM, 'setValue' | 'getValue'> })).GM = {
    async setValue () {
      // Do nothing
    },
    async getValue () {
      return undefined
    }
  }
}

Vue.config.productionTip = false

const menuOptions: JQueryContextMenuOptions[] = []
let menuTrigger = -1
const appProps = Vue.observable({
  actions: {}
})

async function analyzeMenus () {
  const fakeEvent = new Event('click')

  const actionMap: ActionMap = {}

  menuOptions.forEach(options => {
    if (!knownSelector(options.selector)) {
      return
    }
    const type = SELECTOR_TO_ACTION_TYPE[options.selector]
    const trigger = realWindow.$(options.selector)
    const items = options.build?.(trigger, fakeEvent).items
    actionMap[type] = items === undefined ? [] : parseActions(items)
  })
  appProps.actions = actionMap
  menuOptions.splice(0)
  menuTrigger = -1
}

function hook (old: JQueryStatic['contextMenu']): JQueryStatic['contextMenu']
function hook (old: (options?: JQueryContextMenuOptions) => JQuery) {
  return (options?: JQueryContextMenuOptions) => {
    if (options !== undefined) {
      menuOptions.push(options)
      if (menuTrigger === -1) {
        menuTrigger = setTimeout(analyzeMenus)
      }
    }
    return old(options)
  }
}

realWindow.$.contextMenu = hook(realWindow.$.contextMenu)

Vue.mixin({
  beforeCreate () {
    this.$store = appProps
    this.$window = realWindow
  }
})

function replaceUI () {
  document.querySelectorAll('body > div#mhPlay').forEach(element => {
    (element as HTMLDivElement).style.display = 'none'
  })
  const mvvmDiv = document.createElement('div')
  mvvmDiv.id = 'mvvm'
  const mhPlay = document.querySelector('div#mhPlay')
  if (mhPlay) {
    mhPlay.before(mvvmDiv)
  }
  new Vue({
    render: h => h(App)
  }).$mount('#mvvm')
}

if (realWindow.location.toString().match(/MH_Play\/Play_vue.php$/) !== null) {
  replaceUI()
}
