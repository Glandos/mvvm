export type mayBeNumber = string | number | null | undefined

export function parseMayBeNumber (n: mayBeNumber): number {
  return typeof n === 'number' ? n : parseInt(n ?? '')
}

export const GM_STORE_KEYS = {
  scizToken: 'sciz_token',
  bricolCredentials: 'bricol_trolls'
}

export const LS_STORE_KEYS = {
  scizToken: 'mvvm.sciz_token',
  scizTokens: 'mvvm.sciz_tokens',
  bricolCredentials: 'mvvm.bricol_trolls'
}

export function scizHeaders (): Headers[] {
  return JSON.parse(localStorage.getItem(LS_STORE_KEYS.scizTokens) ?? '[]').filter((token: string) => token.length !== 0).map((token: string) => ({
    Authorization: token,
    'Content-type': 'application/json'
  }))
}

export interface BricolOptions {
  name: string
  login: string
  password: string
}

export async function bricolList (): Promise<BricolOptions[]> {
  return JSON.parse(localStorage.getItem(LS_STORE_KEYS.bricolCredentials) ?? await GM.getValue(GM_STORE_KEYS.bricolCredentials) ?? '[]')
}

export type RGBColor = [number, number, number]

export function getColorInGradient (ratio: number, colorStops: Record<number, RGBColor>): RGBColor {
  let [startIndex, startColor] = [0, [0, 0, 0]]
  let [stopIndex, stopColor] = [0, [0, 0, 0]]
  let stopIndexKey = ''
  for ([stopIndexKey, stopColor] of Object.entries(colorStops)) {
    stopIndex = parseInt(stopIndexKey)
    if (stopIndex > ratio || stopIndex === 1) {
      break
    }
    [startIndex, startColor] = [stopIndex, stopColor]
  }
  const multiplier = (ratio - startIndex) / (stopIndex - startIndex)
  const finalColor: RGBColor = [
    startColor[0] + (stopColor[0] - startColor[0]) * multiplier,
    startColor[1] + (stopColor[1] - startColor[1]) * multiplier,
    startColor[2] + (stopColor[2] - startColor[2]) * multiplier
  ]

  return finalColor
}

export const GOOD_HEALTH_COLOR: RGBColor = [0, 174, 0]
export const BAD_HEALTH_COLOR: RGBColor = [174, 0, 0]

export function getHealthColor (ratio: number): RGBColor {
  return getColorInGradient(ratio, {
    0: BAD_HEALTH_COLOR,
    0.4: [215, 105, 8],
    0.6: [221, 207, 0],
    1: GOOD_HEALTH_COLOR
  })
}

export function toColorString (color: RGBColor): string {
  return `rgb(${color.join(',')})`
}
