export type Interval = {
  min?: number
  max?: number
}

export type SpellType = 'Au toucher' | 'Automatique' | 'De zone'

export interface MZMonster {
  id: number
  nom: string
  nCdM: number
  timegmt: number

  fam: string

  niv: Interval
  pv: Interval
  MM: Interval
  RM: Interval
  arm: Interval
  armP: Interval
  armM: Interval
  reg: Interval
  att: Interval
  deg: Interval
  esq: Interval
  vue: Interval
  vlc?: 1
  attd?: 1
  pouv: string
  portpouv: SpellType

  bless?: number
  duree?: Interval
  vit?: string
  gen?: number
}

export type MZResponseObject = MZMonster | { tempsCalculV2: number } | { nbReq: number } | { nbKey: number }
export type MZResponse = MZResponseObject[]

declare module './mhtypes' {
  export interface MonsterProperties {
    mz: MZMonster | null
  }
}

interface BaseMission {
  number: number
  mundidey: boolean
  libelle: string
}

interface RaceMission extends BaseMission {
  type: 'Race'
  race: string
}

interface FamilyMission extends BaseMission {
  type: 'Famille'
  famille: string
}

export type Mission = RaceMission | FamilyMission

export function readMissions (troll: number): Mission[] {
  const rawMissions = localStorage.getItem(`${troll}.MISSIONS`)
  if (rawMissions === null) {
    return []
  }
  const parsedMissions = JSON.parse(rawMissions) as Record<string, Mission>
  return Object.entries(parsedMissions).map(([key, mission]) => {
    mission.number = Number.parseInt(key)
    return mission
  })
}
