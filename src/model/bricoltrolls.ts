
/* eslint-disable camelcase */
export interface BricolTroll {
  id: number
  pv: number
  pv_max: number
  updated_at: string
  dla: string
  pa: 6
  nom: string
  niveau: number
  race: string
  x: number
  y: number
  n: number
  camoufle: boolean
  invisible: boolean
}
/* eslint-enable camelcase */

export interface BricollResponse {
  data: {
    trolls: Record<number, BricolTroll>
  }
}

declare module './mhtypes' {
  export interface TrollProperties {
    camouflaged: boolean | null
    invisible: boolean | null
  }
}
