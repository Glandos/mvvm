import { MHEvents } from './events'
import { Mission } from './mountyzilla'

export class MHType {
  private id: symbol
  protected substringMatch = false
  protected elements: string[]

  constructor (readonly name: string, elements: string[] = []) {
    this.id = Symbol(name)
    this.name = name
    this.elements = elements
  }

  switch (name?: string | null, element?: Element | null, warn = true): this {
    if (name === undefined || name === null) {
      return this
    }
    const longestMatch = {
      matched: '',
      existing: this
    }
    for (const existing of Object.values(this.constructor)) {
      const casted = existing as this
      if (casted.substringMatch) {
        if (name.includes(casted.name) && casted.name.length > longestMatch.matched.length) {
          longestMatch.matched = casted.name
          longestMatch.existing = casted
        }
      } else if (casted.name === name || casted.elements.includes(name)) {
        return casted
      }
    }
    if (longestMatch.matched) {
      return longestMatch.existing
    }
    if (warn) {
      console.warn('Type not found', name)
    }
    return this
  }
}

export class PlaceType extends MHType {
  static UNKNOWN = new PlaceType('')
  static AD_AGENCY = new PlaceType('Agence d\'Annonce')
  static ARMORY = new PlaceType('Armurerie')
  static BURROW = new PlaceType('Terrier')
  static DEMONIC_PORTAL = new PlaceType('Portail Démoniaque')
  static EMPTY_LAIR = new PlaceType('Tanière de trõll')
  static ENCHANTMENT_SHOP = new PlaceType('Boutique d\'Enchantement')
  static EFFUSIVE_CRACK = new PlaceType('Fissure effusive')
  static FORGE = new PlaceType('Forge')
  static GEYSER = new PlaceType('Geyser', ['Geyser de la désolation', "Geyser de l'ombre", 'Geyser du Manger', "Geyser de l'oubli", 'Geyser de la bête', 'Geyser du Feu', 'Geyser de la noirceur', 'Geyser du diable', 'Geyser de la nuit', 'Geyser de la mort', 'Geyser de la victoire', 'Geyser du sage', 'Geyser des fous', 'Geyser du Froid', 'Geyser du Hum', 'Geyser sans nom', 'Geyser du guerrier', 'Geyser des abîmes', 'Geyser du silence', 'Geyser du Bla Bla'])
  static GRINDER_HOUSE = new PlaceType('Cahute du Rémouleur')
  static LAIR = new PlaceType('')
  static LICE = new PlaceType('Lice')
  static MALEFACTORY = new PlaceType('Maléfacterie')
  static METEORITE_HOLE = new PlaceType('Trou de Météorite')
  static MINEROLL = new PlaceType('Minéroll')
  static MISC_SERVICES = new PlaceType('Divers Services')
  static NEST = new PlaceType('Nid')
  static PORTAL_EXIT = new PlaceType('Sortie de Portail')
  static PORTAL = new PlaceType('Portail de Téléportation')
  static PURIFICATION_SPRING = new PlaceType('Source de Purification')
  static SEPULCHRE = new PlaceType('Sépulcre')
  static SHELTER = new PlaceType('Refuge')
  static TANNER = new PlaceType('Tanneur')
  static TELEPORTER = new PlaceType('Téléporteur')
  static TREPANNER = new PlaceType('Antre du trépanateur')
  static TRIPOTROLL = new PlaceType('Tripotroll')
  static WELL = new PlaceType('Puits')

  switch (name?: string | null, element?: Element | null): this {
    const found = super.switch(name, element, false)
    if (found !== this) {
      return found
    }

    if (element instanceof HTMLElement &&
      element.firstElementChild?.getAttribute('href')?.includes('TaniereDescription.php')) {
      return PlaceType.LAIR as this
    }

    console.warn('Type not found', name)
    return this
  }
}

export interface PlaceProperties {
  owner: string | null
  services: string[]
}

export interface MonsterProperties {
  age: string
  template: string | null
  mark: string | null
  missions: Mission[]
  events: MHEvents
}

const COMMON_TEMPLATES = ['Cogneur', 'Coriace', 'Corrompu', 'Gardien', 'Gros', 'Malade', 'Petit', 'Poisseux', 'Ronfleur', 'Trappeur']
abstract class MonsterTypeFeatures {
  static familyName: string
  static ages: string[]
  static templates: string[]
}

class UnknownFeatures {
  static familyName = 'Inconnue'
  static ages = []
  static templates = []
}
class AnimalFeatures implements MonsterTypeFeatures {
  static familyName = 'Animal'
  static ages = ['Bébé', 'Enfançon', 'Jeune', 'Adulte', 'Mature', 'Chef de Harde', 'Ancien', 'Ancêtre']
  static templates = ['Attentionné', 'Enragé', 'Vorace', 'Homochrome', 'Fouisseur', 'Gigantesque', 'Apprivoisé']
}

class HumanoidFeatures implements MonsterTypeFeatures {
  static familyName = 'Humanoïde'
  static ages = ['Nouveau', 'Jeune', 'Adulte', 'Vétéran', 'Briscard', 'Doyen', 'Légendaire', 'Mythique']
  static templates = ['Guérisseur', 'Planqué', 'Alchimiste', 'Agressif', 'Barbare', 'Berserker', 'Champion', 'Effrayé', 'Fou', 'Frondeur', 'Grand Frondeur', 'Guerrier', 'Héros', 'Mutant', 'Paysan', 'Scout', 'Shaman', 'Sorcier', 'Voleur', 'Fanatique']
}

class MonsterFeatures implements MonsterTypeFeatures {
  static familyName = 'Monstre'
  static ages = ['Nouveau', 'Jeune', 'Adulte', 'Vétéran', 'Briscard', 'Doyen', 'Légendaire', 'Mythique']
  static templates = ['Fouisseur', 'Gigantesque', 'Colossal', 'Cracheur', 'Esculape', 'Frénétique', 'Fustigateur', 'Gargantuesque', 'Homomorphe', 'Traqueur']
}

class DemonFeatures implements MonsterTypeFeatures {
  static familyName = 'Démon'
  static ages = ['Initial', 'Novice', 'Mineur', 'Favori', 'Majeur', 'Supérieur', 'Suprême', 'Ultime']
  static templates = ['de Premier Cercle', 'de Second Cercle', 'de Troisième Cercle', 'de Quatrième Cercle', 'de Cinquième Cercle', 'des Abysses', 'Prince', 'Fanatique']
}

class InsectFeatures implements MonsterTypeFeatures {
  static familyName = 'Insecte'
  static ages = ['Larve', 'Immature', 'Juvénile', 'Imago', 'Développé', 'Mûr', 'Accompli', 'Achevé']
  static templates = ['Homochrome', 'Fouisseur', 'Gigantesque', 'Alpha', 'Lobotomisateur', 'Morticole', 'Ouvrier', 'Reine', 'Soldat', 'Strident']
}

class UndeadFeatures implements MonsterTypeFeatures {
  static familyName = 'Mort-vivant'
  static ages = ['Naissant', 'Recent', 'Ancien', 'Vénérable', 'Séculaire', 'Antique', 'Ancestral', 'Antédiluvien']
  static templates = ['Archaique', 'Implacable', 'Maitre', 'Médicastre', 'Mentat', 'Nécromant', 'Psychophage', 'Spectral']
}

export class MonsterType extends MHType {
  static UNKNOWN = new MonsterType('')

  static ABISHAII_BLEU = new MonsterType('Abishaii Bleu', 'GO_abishai_bleu.jpg', DemonFeatures, 'Bibi Bleu')
  static ABISHAII_NOIR = new MonsterType('Abishaii Noir', 'GO_abishai_noir.jpg', DemonFeatures, 'Bibi Noir')
  static ABISHAII_ROUGE = new MonsterType('Abishaii Rouge', 'GO_abishai_rouge.jpg', DemonFeatures, 'Bibi Rouge')
  static ABISHAII_VERT = new MonsterType('Abishaii Vert', 'GO_abishai_vert.jpg', DemonFeatures, 'Bibi Vert')
  static AME_EN_PEINE = new MonsterType('Ame-en-peine', 'BO_AmeEnPeine.jpg', UndeadFeatures, 'Âme')
  static AMIBE_GEANTE = new MonsterType('Amibe Géante', 'VB_amibe.jpg', MonsterFeatures, 'Amibe')
  static ANACONDA_DES_CATACOMBES = new MonsterType('Anaconda des Catacombes', 'AnacondaCatacombes.jpg', MonsterFeatures, 'Anaconda')
  static ANKHEG = new MonsterType('Ankheg', 'VG_Ankheg.jpg', InsectFeatures)
  static ANOPLOURE_PURPURIN = new MonsterType('Anoploure Purpurin', 'BO_AnolporPurpurin.jpg', InsectFeatures, 'Anopl.')
  static ARAGNAROK_DU_CHAOS = new MonsterType('Aragnarok du Chaos', 'BO_AragnarokDuChaos..jpg', InsectFeatures, 'Aragnarok')
  static ARAIGNEE_GEANTE = new MonsterType('Araignée Géante', 'AraigneeGeante.jpg', InsectFeatures, 'Araignée')
  static ASHASHIN = new MonsterType('Ashashin', 'BO_Ashashin.jpg', HumanoidFeatures)
  static BALROG = new MonsterType('Balrog', undefined, DemonFeatures)
  static BANSHEE = new MonsterType('Banshee', 'GO_Banshee.jpg', UndeadFeatures)
  static BARGHEST = new MonsterType('Barghest', 'BO_Barghest.jpg', DemonFeatures)
  static BASILISK = new MonsterType('Basilisk', 'BO_Basilisk.jpg', MonsterFeatures)
  static BEHEMOTH = new MonsterType('Behemoth', 'BO_Behemoth.jpg', DemonFeatures)
  static BEHIR = new MonsterType('Behir', 'BO_Behir.jpg', MonsterFeatures)
  static BOGGART = new MonsterType('Boggart', 'GO_Boggart.jpg', HumanoidFeatures)
  static BONDIN = new MonsterType('Bondin', 'VB_Bondin.jpg', MonsterFeatures)
  static BOUJ_DLA = new MonsterType('Bouj\'Dla', 'VB_BoujDla.jpg', MonsterFeatures, 'Bouj\'')
  static BOUJ_DLA_PLACIDE = new MonsterType('Bouj\'Dla Placide', 'VB_BoujDla.jpg', MonsterFeatures, 'Bouj\' Pla.')
  static BULETTE = new MonsterType('Bulette', 'BO_Bulet.jpg', MonsterFeatures)
  static CAILLOUTEUX = new MonsterType('Caillouteux', 'VG_Caillouteux.jpg', HumanoidFeatures)
  static CAPITAN = new MonsterType('Capitan', 'BO_Capitan.jpg', UndeadFeatures)
  static CARNOSAURE = new MonsterType('Carnosaure', 'VG_Carnosaure.jpg', MonsterFeatures)
  static CHAMPI_GLOUTON = new MonsterType('Champi-Glouton', 'ChampiGlouton.jpg', HumanoidFeatures)
  static CHAUVE_SOURIS_GEANTE = new MonsterType('Chauve-Souris Géante', 'ChauveSouris.jpg', AnimalFeatures, 'Chauv\'')
  static CHEVAL_A_DENTS_DE_SABRE = new MonsterType('Cheval à Dents de Sabre', 'MA_cheval_dent_de_sabreF.jpg', AnimalFeatures, 'Cheval')
  static CHIMERE = new MonsterType('Chimère', 'BO_Chimere.jpg', MonsterFeatures)
  static CHONCHON = new MonsterType('Chonchon', 'GO_Chonchon.jpg', MonsterFeatures)
  static COCCICRUELLE = new MonsterType('Coccicruelle', 'FF_coccicruelle.png', InsectFeatures, 'Cocci.')
  static COCKATRICE = new MonsterType('Cockatrice', 'VB_Coquatrice.jpg', MonsterFeatures)
  static CRASC = new MonsterType('Crasc', 'GO_Crasc.jpg', MonsterFeatures)
  static CRASC_MAEXUS = new MonsterType('Crasc Maexus', 'GO_Crasc.jpg', MonsterFeatures)
  static CRASC_MEDIUS = new MonsterType('Crasc Médius', 'GO_Crasc.jpg', MonsterFeatures)
  static CRASC_PARASITUS = new MonsterType('Crasc Parasitus', 'GO_Crasc.jpg', MonsterFeatures)
  static CROQUEMITAINE = new MonsterType('Croquemitaine', 'BO_Croquemitaine.jpg', UndeadFeatures, 'Croquem.')
  static CUBE_GELATINEUX = new MonsterType('Cube Gélatineux', 'OS_Cube_Gelatineux.jpg', MonsterFeatures, 'Cube G.')
  static DIABLOTIN = new MonsterType('Diablotin', 'Diablotin.jpg', DemonFeatures, 'Diabl.')
  static DINDON = new MonsterType('Dindon', 'turkey_03.gif', AnimalFeatures)
  static DAEMONITE = new MonsterType('Daemonite', 'KH_Daemonite.jpg', DemonFeatures, 'Daem.')
  static DJINN = new MonsterType('Djinn', 'VB_djinn.jpg', MonsterFeatures)
  static ECTOPLASME = new MonsterType('Ectoplasme', 'BO_Ectoplasme.jpg', UndeadFeatures, 'Ecto.')
  static EFFRIT = new MonsterType('Effrit', 'KH_Effrit.jpg', MonsterFeatures)
  static ELEMENTAIRE_D_AIR = new MonsterType('Elémentaire d\'Air', 'AK_elementaireair.jpg', DemonFeatures, 'Élém. Air')
  static ELEMENTAIRE_D_EAU = new MonsterType('Elémentaire d\'Eau', 'AK_elementaireeau.jpg', DemonFeatures, 'Élém. Eau')
  static ELEMENTAIRE_DE_FEU = new MonsterType('Elémentaire de Feu', 'AK_elementairefeu.jpg', DemonFeatures, 'Élém. Feu')
  static ELEMENTAIRE_DE_TERRE = new MonsterType('Elémentaire de Terre', 'VB_elementairedeterre.jpg', DemonFeatures, 'Élém. Terre')
  static ELEMENTAIRE_DU_CHAOS = new MonsterType('Elémentaire du Chaos', 'AK_elementairechaos.jpg', DemonFeatures, 'Élém. Chaos')
  static ELEMENTAIRE_MAGMATIQUE = new MonsterType('Elémentaire Magmatique', 'AK_elementairemagmatique.jpg', DemonFeatures, 'Élém. Magma.')
  static ERINYES = new MonsterType('Erinyes', 'VB_erinyes.jpg', DemonFeatures)
  static ESPRIT_FOLLET = new MonsterType('Esprit-Follet', 'FF_follet.png', MonsterFeatures, 'Esprit F.')
  static ESSAIM_CRATERIEN = new MonsterType('Essaim Cratérien', undefined, InsectFeatures, 'Essaim Crat.')
  static ESSAIM_SANGUINAIRE = new MonsterType('Essaim Sanguinaire', 'FF_essaim-sanguinaire.png', InsectFeatures, 'Essaim Sang.')
  static ETTIN = new MonsterType('Ettin', 'VB_etins.jpg', HumanoidFeatures)
  static FANTOME = new MonsterType('Fantôme', 'GO_Fantome.jpg', UndeadFeatures)
  static FEU_FOLLET = new MonsterType('Feu Follet', 'VB_feufollet.jpg', MonsterFeatures)
  static FLAGELLEUR_MENTAL = new MonsterType('Flagelleur Mental', 'FF_flagelleur.png', HumanoidFeatures, 'Flagelleur')
  static FOUDROYEUR = new MonsterType('Foudroyeur', 'TO_Foudroyeur.jpg', InsectFeatures)
  static FUMEUX = new MonsterType('Fumeux', 'Fumeux.jpg', DemonFeatures)
  static FUNGUS_GEANT = new MonsterType('Fungus Géant', 'OF_Fungus.jpg', MonsterFeatures, 'Fungus G.')
  static FUNGUS_VIOLET = new MonsterType('Fungus Violet', 'KJ_FungusViolet.jpg', MonsterFeatures, 'Fungus V.')
  static FURGOLIN = new MonsterType('Furgolin', 'VB_Furgolin.jpg', HumanoidFeatures)
  static GARGOUILLE = new MonsterType('Gargouille', 'VB_Gargouille.jpg', MonsterFeatures)
  static GEANT_DE_PIERRE = new MonsterType('Géant de Pierre', 'BO_GeantDePierre.jpg', HumanoidFeatures, 'Gé. de Pierre')
  static GEANT_DES_GOUFFRES = new MonsterType('Géant des Gouffres', 'BO_GeantDesGouffres.jpg', HumanoidFeatures, 'Gé. des Gouffres')
  static GECK_OO = new MonsterType('Geck\'oo', undefined, AnimalFeatures)
  static GECK_OO_MAJESTUEUX = new MonsterType('Geck\'oo Majestueux', undefined, AnimalFeatures, 'Geck\'oo Maj.')
  static GLOUTON = new MonsterType('Glouton', 'OF_Glouton.jpg', AnimalFeatures)
  static GNOLL = new MonsterType('Gnoll', 'FP_Gnoll.jpg', HumanoidFeatures)
  static GNU_SAUVAGE = new MonsterType('Gnu Sauvage', 'GnuSauvage.jpg', AnimalFeatures, 'Gnu Sauv.')
  static GOBLIN = new MonsterType('Goblin', 'Gobelin.jpg', HumanoidFeatures)
  static GOBLOURS = new MonsterType('Goblours', 'OF_Goblours.jpg', HumanoidFeatures)
  static GOLEM_D_ARGILE = new MonsterType('Golem d\'Argile', 'BO_GolemDArgile.jpg', HumanoidFeatures)
  static GOLEM_DE_CHAIR = new MonsterType('Golem de Chair', 'BO_GolemDeChair.jpg', HumanoidFeatures)
  static GOLEM_DE_FER = new MonsterType('Golem de Fer', 'OF_GolemFer.jpg', HumanoidFeatures)
  static GOLEM_DE_PIERRE = new MonsterType('Golem de Pierre', 'BO_GolemDePierre.jpg', HumanoidFeatures)
  static GORGONE = new MonsterType('Gorgone', 'VG_Gorgogne.jpg', MonsterFeatures)
  static GOULE = new MonsterType('Goule', 'FP_Goule.jpg', UndeadFeatures)
  static GREMLINS = new MonsterType('Gremlins', 'Gremlins.jpg', HumanoidFeatures)
  static GRITCHE = new MonsterType('Gritche', undefined, DemonFeatures)
  static GROUILLEUX = new MonsterType('Grouilleux', undefined, MonsterFeatures)
  static GRYLLE = new MonsterType('Grylle', 'OF_Grylle.jpg', MonsterFeatures)
  static HARPIE = new MonsterType('Harpie', 'Harpie.jpg', MonsterFeatures)
  static HELLROT = new MonsterType('Hellrot', 'VB_hellrot.jpg', DemonFeatures)
  static HOMME_LEZARD = new MonsterType('Homme-Lézard', 'VB_homme-lezard.jpg', HumanoidFeatures)
  static HURLEUR = new MonsterType('Hurleur', 'Hurleur.jpg', HumanoidFeatures)
  static INCUBE = new MonsterType('Incube', 'BO_Incube.jpg', DemonFeatures)
  static KOBOLD = new MonsterType('Kobold', 'VG_Kobold.jpg', HumanoidFeatures)
  static LABEILLEUX = new MonsterType('Labeilleux', 'OF_Labeilleux.jpg', InsectFeatures)
  static LEZARD_GEANT = new MonsterType('Lézard Géant', 'LezardGeant.jpg', MonsterFeatures, 'Lézard')
  static LIMACE_GEANTE = new MonsterType('Limace Géante', 'VB_limace.jpg', InsectFeatures, 'Limace')
  static LOUP_GAROU = new MonsterType('Loup-Garou', 'LoupGarou.jpg', HumanoidFeatures)
  static LUTIN = new MonsterType('Lutin', 'Lutin.jpg', HumanoidFeatures)
  static MANTE_FULCREUSE = new MonsterType('Mante Fulcreuse', undefined, InsectFeatures)
  static MANTICORE = new MonsterType('Manticore', 'VB_manticore.jpg', MonsterFeatures)
  static MARILITH = new MonsterType('Marilith', 'GO_Marilith2.jpg', DemonFeatures)
  static MEDUSE = new MonsterType('Méduse', 'VB_Meduse.jpg')
  static MEGACEPHALE = new MonsterType('Mégacéphale', 'FP_Megacephale.jpg', HumanoidFeatures, 'Méga.')
  static MILLE_PATTES_GEANT = new MonsterType('Mille-Pattes Géant', 'VG_MillePattesGeant.jpg')
  static MIMIQUE = new MonsterType('Mimique', 'VG_Mimique.jpg')
  static MINOTAURE = new MonsterType('Minotaure', 'Minotaure.jpg')
  static MOHRG = new MonsterType('Mohrg', 'FF_mohrg.png')
  static MOLOSSE_SATANIQUE = new MonsterType('Molosse Satanique', 'MolosseSatanique.jpg', DemonFeatures)
  static MOMIE = new MonsterType('Momie', 'Momie.jpg')
  static MONSTRE_ROUILLEUR = new MonsterType('Monstre Rouilleur', 'VB_monstre_rouilleur.jpg')
  static MOUCH_OO_MAJESTUEUX_SAUVAGE = new MonsterType('Mouch\'oo Majestueux Sauvage')
  static MOUCH_OO_SAUVAGE = new MonsterType('Mouch\'oo Sauvage', 'NA_Mouchoo.gif')
  static NAGA = new MonsterType('Naga', 'TO_Naga.jpg')
  static NECROCHORE = new MonsterType('Nécrochore', 'KH_Necrochore.jpg', UndeadFeatures, 'Nécroch.')
  static NECROMANT = new MonsterType('Nécromant', 'OS_Necromant.png', UndeadFeatures, 'Nécrom.')
  static NECROPHAGE = new MonsterType('Nécrophage')
  static NUAGE_D_INSECTES = new MonsterType('Nuage d\'Insectes')
  static NUEE_DE_VERMINE = new MonsterType('Nuée de Vermine', 'FF_nuee-vermines.png')
  static OGRE = new MonsterType('Ogre', 'Ogre.jpg')
  static OMBRE = new MonsterType('Ombre', 'VB_ombre.jpg')
  static OMBRE_DE_ROCHES = new MonsterType('Ombre de Roches', 'VG_OmbreDesRoches.jpg')
  static ORQUE = new MonsterType('Orque', 'Orque.jpg')
  static OURS_GAROU = new MonsterType('Ours-Garou', 'GO_OursGarou.jpg')
  static PALEFROI_INFERNAL = new MonsterType('Palefroi Infernal', 'VG_PalefroiInfernal.jpg', DemonFeatures, 'Palefroi')
  static PHOENIX = new MonsterType('Phoenix', 'GO_Phoenix.jpg')
  static PITITABEILLE = new MonsterType('Pititabeille', 'AS_pititeNabeille.jpg', InsectFeatures, 'Pititab.')
  static PLANTE_CARNIVORE = new MonsterType('Plante Carnivore', 'PlanteCarnivore.jpg')
  static PSEUDO_DRAGON = new MonsterType('Pseudo-Dragon', 'OF_PseudoDragon.jpg', DemonFeatures)
  static RAQUETTOU = new MonsterType('Raquettou', 'OS_Racketou.png')
  static RAT_GEANT = new MonsterType('Rat Géant', 'RatGeant.jpg')
  static RAT_GAROU = new MonsterType('Rat-Garou', 'RatGarou.jpg')
  static ROCKETEUX = new MonsterType('Rocketeux', 'VB_rocketeux.jpg')
  static SAGOUIN = new MonsterType('Sagouin', 'VG_Sagouin.jpg')
  static SCARABEE_GEANT = new MonsterType('Scarabée Géant', 'ScarabeeGeant.jpg')
  static SCORPION_GEANT = new MonsterType('Scorpion Géant', 'VG_ScorpionGeant.jpg')
  static SHAI = new MonsterType('Shai', 'GO_Shai.jpg', DemonFeatures)
  static SLAAD = new MonsterType('Slaad', 'Slaad.jpg')
  static SORCIERE = new MonsterType('Sorcière', 'VB_sorciere.jpg')
  static SPECTRE = new MonsterType('Spectre', 'OF_Spectre.jpg', UndeadFeatures)
  static SPHINX = new MonsterType('Sphinx')
  static SQUELETTE = new MonsterType('Squelette', 'Squelette.jpg')
  static STRIGE = new MonsterType('Strige', 'VB_strige.jpg', InsectFeatures)
  static SUCCUBE = new MonsterType('Succube', 'Succube.jpg', DemonFeatures)
  static TERTRE_ERRANT = new MonsterType('Tertre Errant', 'BO_TertreErrant.jpg')
  static THRI_KREEN = new MonsterType('Thri-kreen', 'VB_three-kreen.jpg')
  static TIGRE_GAROU = new MonsterType('Tigre-Garou', 'KH_TigreGarou.jpg')
  static TITAN = new MonsterType('Titan', 'VB_titan.jpg')
  static TRANCHEUR = new MonsterType('Trancheur', 'VB_petittrancheur.jpg', MonsterFeatures)
  static TUBERCULE_TUEUR = new MonsterType('Tubercule Tueur', 'GO_Tubercule.jpg')
  static TUTOKI = new MonsterType('Tutoki', 'FP_Tutoki.jpg')
  static VAMPIRE = new MonsterType('Vampire', 'GO_Vampire.jpg')
  static VESKAN_DU_CHAOS = new MonsterType('Veskan du Chaos', 'BO_VeskanDuChaos.jpg')
  static VER_CARNIVORE_GEANT = new MonsterType('Ver Carnivore Géant', 'GO_Vcg.jpg', MonsterFeatures, 'Ver Carn.')
  static VOUIVRE = new MonsterType('Vouivre', 'VG_Vouivre.jpg')
  static WORG = new MonsterType('Worg', 'FF_worg.png', MonsterFeatures)
  static XORN = new MonsterType('Xorn', 'OF_Xorn.jpg', DemonFeatures)
  static YETI = new MonsterType('Yéti', 'Yeti.jpg')
  static YUAN_TI = new MonsterType('Yuan-ti', 'FF_yuanti.png')
  static ZOMBIE = new MonsterType('Zombie', 'Zombie.jpg')

  static GOWAP = new MonsterType('Gowap', 'BO_Gowap.jpg', AnimalFeatures)
  static GNU_DOMESTIQUE = new MonsterType('Gnu Domestique', 'GnuSauvage.jpg', AnimalFeatures, 'Gnu Dom.')
  static FAMILIER = new MonsterType('Familier', 'VB_familier.jpg', MonsterFeatures)
  static GOLEM_DE_CUIR = new MonsterType('Golem de Cuir', undefined, MonsterFeatures)
  static GOLEM_DE_METAL = new MonsterType('Golem de Métal', undefined, MonsterFeatures)
  static GOLEM_DE_MITHRIL = new MonsterType('Golem de Mithril', undefined, MonsterFeatures)
  static GOLEM_DE_PAPIER = new MonsterType('Golem de Papier', undefined, MonsterFeatures)

  protected substringMatch = true

  featuresRegexp: RegExp
  readonly imageURI?: string

  constructor (name: string, image?: string, readonly features: typeof MonsterTypeFeatures = UnknownFeatures, readonly nickname: string = name) {
    super(name)
    this.imageURI = image !== undefined ? `https://www.mountyhall.com/images/Monstres/${image}` : undefined

    const templatesJoin = [...COMMON_TEMPLATES, ...features.templates].join('|')
    this.featuresRegexp = new RegExp(String.raw`^(?:(?<template_prefix>${templatesJoin}) )?${name}(?: (?<template_suffix>${templatesJoin}))? \[(?<age>${features.ages.join('|')})\](?: (?<mark>.*))?$`)
  }

  extractFeatures (completeName: string): Pick<MonsterProperties, 'age' | 'template' | 'mark'> {
    const extracted = completeName.match(this.featuresRegexp)?.groups ?? {}
    return {
      age: extracted.age ?? null,
      template: extracted.template_prefix ?? extracted.template_suffix ?? null,
      mark: extracted.mark ?? null
    }
  }
}

export interface TreasureProperties {
  _placeholder: never
}

export class TreasureType extends MHType {
  static UNKNOWN = new TreasureType('')

  static OUTIL = new TreasureType('Outil', ['Alêne', 'Burin', 'Ciseau à bois', 'Marteau de forgeron', "Marteau d'orfèvre", 'Plioir'])
  static ANNEAU = new TreasureType('Anneau', ['Anneau de protection'])
  static APOCRYPHE = new TreasureType('Apocryphe', ['Apocryphe de Décomposition', 'Apocryphe du Phoenix'])
  static ARMURE = new TreasureType('Armure', ['Armure de bois', 'Armure de cuir', 'Armure de peaux', 'Armure de pierre', 'Armure de plates', "Armure d'anneaux", 'Cotte de mailles', 'Cuir bouilli', "Cuirasse d'ossements", "Cuirasse d'écailles", 'Culotte en cuir', 'Fourrures', "Haubert d'écailles", 'Haubert de mailles', 'Pagne de mailles', 'Pagne en cuir', 'Robe de mage', 'Tunique', "Tunique d'écailles"])
  static ARME_2_MAINS = new TreasureType('Arme (2 mains)', ['Baton lesté', 'Bâtons de parade', 'Chaîne cloutée', 'Espadon', 'Grosse stalagmite', "Hache à deux mains d'obsidienne", 'Hache de bataille', 'Hache de guerre en os', 'Hache de guerre en pierre', 'Hallebarde'])
  static BOTTES = new TreasureType('Bottes', ['Bottes', 'Jambières en cuir', 'Jambières en fourrure', 'Jambières en maille', 'Jambières en métal', 'Jambières en os', 'Sandales', 'Souliers dorés'])
  static BOUCLIER = new TreasureType('Bouclier', ['Bouclier à pointes', 'Grimoire', "Gros'Porte", 'Rondache en bois', 'Rondache en métal', 'Targe'])
  static ARME_1_MAIN = new TreasureType('Arme (1 main)', ['Boulet et chaîne', "Coutelas d'obsidienne", 'Coutelas en os', 'Crochet', 'Dague', 'Epée courte', 'Epée longue', 'Fouet', 'Gantelet', 'Gourdin', 'Gourdin clouté', 'Grosse racine', "Lame d'obsidienne", 'Lame en os', 'Lame en pierre', 'Machette', "Masse d'arme", 'Torche'])
  static CASQUE = new TreasureType('Casque', ['Cagoule', 'Casque à cornes', 'Casque à pointes', 'Casque en cuir', 'Casque en métal', 'Chapeau pointu', 'Couronne de cristal', "Couronne d'obsidienne", 'Heaume', 'Lorgnons', 'Turban'])
  static CARTE = new TreasureType('Carte', ['Carte des Raccourcis :'])
  static TALISMAN = new TreasureType('Talisman', ['Collier à pointes', 'Collier de dents', 'Collier de pierre', 'Gorgeron en cuir', 'Gorgeron en métal', 'Talisman de pierre', "Talisman d'obsidienne", 'Torque de pierre'])
  static CONTENEUR = new TreasureType('Conteneur', ['Conteneur'])
  static COQUILLAGE = new TreasureType('Coquillage', ['Coquillage'])
  static MINERAI = new TreasureType('Minerai', ['Cristal de Sâl', 'Flaaschy', 'Kibrille', 'Obsidienne', 'Woeepaah'])
  static POTION = new TreasureType('Potion', ['Dover Powa', 'Elixir de Bonne Bouffe', 'Elixir de Corruption', 'Elixir de Fertilité', 'Elixir de Feu', 'Elixir de Longue-Vue', 'Essence de KouleMann', 'Extrait de DjhinTonik', 'Extrait du Glacier', 'Grippe en Conserve', 'Jus de Cervelle', 'Jus de Chronomètre', 'Lampe Géniale', 'Métomol', 'Pneumonie en Conserve', 'Potion de Guérison', 'Potion de Pàïntûré', 'PufPuff', 'Rhume en Conserve', 'Sang de Toh Réroh', 'Sinne Khole', 'Toxine Violente', "Voï'Pu'Rin", 'Zet Crakdedand'])
  static PARCHEMIN = new TreasureType('Parchemin', ['Idées Confuses', 'Invention Extraordinaire', 'Mission', 'Parchemin Gribouillé', 'Parchemin Vierge', 'Plan Génial', 'Rune des Cyclopes', 'Rune des Foins', 'Rune Explosive', 'Sortilège', 'Traité de Clairvoyance', "Yeu'Ki'Pic"])
  static TETE_REDUITE = new TreasureType('Tête Réduite', ['Tête Réduite'])

  static COMPOSANT = new TreasureType('Composant')
  static GIGOT_DE_GOB_WITH_AMOUNT = new TreasureType('GG\'', [], 'GG’', true)
  static GIGOT_DE_GOB_WITHOUT_AMOUNT = new TreasureType('Gigots de Gob', [], 'GG’')
  static SPECIAL = new TreasureType('Spécial', ["Poiscaille d'Avril"])

  constructor (name: string, readonly elements: string[] = [], readonly nickname = name, substringMatch = false) {
    super(name, elements)
    this.substringMatch = substringMatch
  }
}

export class TrollRace extends MHType {
  static UNKNOWN = new TrollRace('')
  static DURAKUIR = new TrollRace('Durakuir')
  static KASTAR = new TrollRace('Kastar')
  static TOMAWAK = new TrollRace('Tomawak')
  static SKRIM = new TrollRace('Skrim')
  static DARKLING = new TrollRace('Darkling')
}

export class Trolligion extends MHType {
  static UNKNOWN = new Trolligion('')
  static FROID = new Trolligion('Prieur du Dieu du Froid')
  static MANGER = new Trolligion('Prieur de la Déesse du Manger')
}

export interface TrollProperties {
  guild: string | null
  level: number
  trolligion: Trolligion
  hidden: boolean
  tags: {
    image: string
    title: string
  }[]
  // sciz: SCIZTroll | null
  events: MHEvents
}

interface BaseObject<T, E> {
  type: T
  extended: E
}

export type Troll = BaseObject<TrollRace, TrollProperties>
export type Monster = BaseObject<MonsterType, MonsterProperties>
export type Place = BaseObject<PlaceType, PlaceProperties>
export type Treasure = BaseObject<TreasureType, TreasureProperties>
