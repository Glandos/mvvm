import { mayBeNumber, parseMayBeNumber } from '@/utils'

import { Troll, Place, Treasure, Monster } from './mhtypes'

export class Position {
  readonly x = NaN
  readonly y = NaN
  readonly n = NaN
  constructor (x: mayBeNumber = NaN, y: mayBeNumber = NaN, n: mayBeNumber = NaN) {
    this.x = parseMayBeNumber(x)
    this.y = parseMayBeNumber(y)
    this.n = parseMayBeNumber(n)
  }

  distance (point: Position): number {
    return Math.max(...[point.x - this.x, point.y - this.y, point.n - this.n].map(Math.abs))
  }
}

export type KnownObjects = Troll | Place | Treasure | Monster

export type ExtractType<T extends KnownObjects> = T['type']
export type ExtractExtended<T extends KnownObjects> = T['extended']

export class ModelObject<T extends KnownObjects> {
  id: number
  name: string
  type: ExtractType<T>
  position: Position
  distance: number
  extended: ExtractExtended<T>

  constructor (
    id: mayBeNumber,
    name: string | undefined,
    type: ExtractType<T>,
    position: Position,
    distance: mayBeNumber,
    extended: ExtractExtended<T>
  ) {
    this.id = parseMayBeNumber(id)
    this.type = type
    this.name = name ?? ''
    this.position = position
    this.distance = parseMayBeNumber(distance)

    this.extended = extended
  }
}

export class ModelObjects<T extends KnownObjects> {
  [id: number]: ModelObject<T>

  constructor (objects?: ModelObject<T>[]) {
    if (objects === undefined) {
      return
    }
    objects.forEach(o => (this[o.id] = o))
  }

  all (): ModelObject<T>[] {
    return Array.from(Object.values(this))
  }

  sorted (): ModelObject<T>[] {
    return Array.from(Object.values(this)).sort(
      (a, b) => {
        return a.distance - b.distance
      }
    )
  }
}

export const MODEL_KEYS = ['trolls', 'treasures', 'monsters', 'places'] as const

export class Model {
  position: Position
  distance = 0
  monsters: ModelObjects<Monster>
  treasures: ModelObjects<Treasure>
  places: ModelObjects<Place>
  trolls: ModelObjects<Troll>
  loading: boolean
  me: number

  constructor () {
    this.me = NaN
    this.position = new Position()
    this.monsters = new ModelObjects()
    this.trolls = new ModelObjects()
    this.places = new ModelObjects()
    this.treasures = new ModelObjects()
    this.loading = true
  }

  sorted (): ModelObject<KnownObjects>[] {
    return Array.from(Object.values(this.monsters))
      .concat(Object.values(this.trolls))
      .concat(Object.values(this.places))
      .concat(Object.values(this.treasures))
      .sort(
        (a: ModelObject<KnownObjects>, b: ModelObject<KnownObjects>) => {
          return a.distance - b.distance
        }
      )
  }

  private get allParts () {
    return [this.monsters, this.trolls, this.places, this.treasures]
  }

  get furthestObject (): number {
    let distance = 0
    for (const part of this.allParts) {
      distance = Math.max(Object.values(part).reduce((previous, current) => Math.max(previous, current.distance), 0), distance)
    }
    return distance
  }
}
