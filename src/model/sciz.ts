import { scizHeaders } from '@/utils'

export interface SCIZTroll {
  id: number;
  pa: number;
  fatigue: number;
  pdv: number;
  pdv_max: number; // eslint-disable-line camelcase
}

export interface SCIZTreasure {
  id: number
  nom: string
  effet: string
  mithril: false
  templates: string | null
  type: string
}

declare module './mhtypes' {
  export interface TrollProperties {
    sciz: SCIZTroll | null
  }

  export interface TreasureProperties {
    sciz: SCIZTreasure | null
  }
}

/* eslint-disable camelcase */
export type SCIZBaseEvent = {
  time: string
  icon: string
  message: string
  owner_id: number
  owner_nom: string
}

export type SCIZCDMEvent = SCIZBaseEvent & {
  mob_id: number
  mob_nom: string
}

export type SCIZAttackEvent = SCIZBaseEvent & {
  att_id: string
  att_nom: string
  def_id: string
  def_nom: string
}
/* eslint-enable camelcase */

export type SCIZEvent = SCIZCDMEvent | SCIZAttackEvent

export interface SCIZResponse {
  trolls?: Array<SCIZTroll>
  treasures?: Array<SCIZTreasure>
  events?: Array<SCIZEvent>
}

export async function mergeSCIZRequests (input: RequestInfo, init?: RequestInit): Promise<SCIZResponse> {
  const responses = scizHeaders().map(async header => {
    const response = await fetch(new Request(input, {
      ...init,
      headers: {
        ...init?.headers,
        ...header
      }
    })
    )

    if (!response.ok) {
      console.error('Error from SCIZ', response)
      return {}
    }
    return response.json() as SCIZResponse
  })

  const mergedResponse = (await Promise.allSettled(responses)).map(promise => promise.status === 'fulfilled' ? promise.value : {})
  return mergedResponse.reduce((mergedResponse, response) => (
    {
      trolls: response.trolls === undefined ? mergedResponse.trolls : (mergedResponse.trolls ?? []).concat(response.trolls),
      treasures: response.treasures === undefined ? mergedResponse.treasures : (mergedResponse.treasures ?? []).concat(response.treasures),
      events: response.events === undefined ? mergedResponse.events : (mergedResponse.events ?? []).concat(response.events)
    }
  ))
}
