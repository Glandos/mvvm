export const SELECTOR_TO_ACTION_TYPE = {
  '.cm-attCac': 'closeAttack',
  '.cm-attDist': 'distantAttack',
  '.cm-troll': 'troll',
  '.cm-monstre': 'monster',
  '.cm-tresor': 'treasure',
  '.cm-champi': 'mushroom',
  '.cm-ceno': 'cenotaph'
} as const

export type ActionType = (typeof SELECTOR_TO_ACTION_TYPE)[keyof typeof SELECTOR_TO_ACTION_TYPE]
export type SelectorType = keyof typeof SELECTOR_TO_ACTION_TYPE

export function knownSelector (selector: string): selector is SelectorType {
  return Object.getOwnPropertyNames(SELECTOR_TO_ACTION_TYPE).includes(selector)
}

export interface Action {
  id: number
  name: string
  actionPoints: number,
  disabled: boolean
}

export type ActionMap = {
  [T in ActionType]?: Array<Action>
}

interface MenuItem {
  name: string
  disabled?: boolean
}

type MenuItems = Record<number, MenuItem | string>

export function parseActions (items: MenuItems): Array<Action> {
  const actionPointsMatcher = /(.*) \((\d) PA\)/

  return Object.entries(items).filter(([, name]) => name !== '---------')
    .map(([id, item]): Action => {
      const originalName = typeof item === 'string' ? item : item.name
      const disabled = typeof item === 'string' ? false : item.disabled ?? false
      const [name, actionsPoint] = originalName.match(actionPointsMatcher)?.slice(1) ?? [originalName, '0']
      const action = {
        id: Number.parseInt(id),
        name,
        actionPoints: Number.parseInt(actionsPoint),
        disabled
      }
      return action
    })
}
