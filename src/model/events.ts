import { parse } from 'date-fns'
import { SCIZEvent } from './sciz'

export class MHEvent {
  date: Date
  type: string
  description: string
  scizEvent: SCIZEvent | null = null

  constructor (date: string | Date, type: string, description: string) {
    if (typeof date === 'string') {
      const [year, monthIndex, day, hours, minutes, seconds] = [
        date.slice(6, 10), date.slice(3, 5), date.slice(0, 2), date.slice(11, 13), date.slice(14, 16), date.slice(17, 19)
      ].map(s => parseInt(s))
      this.date = new Date(year, monthIndex, day, hours, minutes, seconds)
    } else {
      this.date = new Date(date.getTime())
    }
    this.type = type
    this.description = description
  }
}

export class MHEvents extends Array<MHEvent> {
  static EVENT_TIME_TOLERANCE = 2000

  lastRefresh = new Date(0)

  attachEvents = (scizEvents: SCIZEvent[]): void => {
    // Ensure we are sorted
    this.sort()
    for (const scizEvent of scizEvents) {
      const scizDate = parse(scizEvent.time, 'dd/MM/yyyy HH:mm:ss', new Date()).getTime()
      const matchingEvents = this.map(event => [scizDate - event.date.getTime(), event] as [number, MHEvent])
        .filter(([delta, event]) => Math.abs(delta) < MHEvents.EVENT_TIME_TOLERANCE && event.scizEvent === null)
        .sort(([delta1], [delta2]) => delta2 - delta1)
        .map(([, event]) => event)
      if (matchingEvents.length === 0) {
        console.warn('No matching event for', scizEvent)
      } else {
        matchingEvents[0].scizEvent = scizEvent
      }
    }
  }

  sort = (): this => {
    super.sort((e1, e2) => e1.date.getTime() - e2.date.getTime())
    return this
  }
}
