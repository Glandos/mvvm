import { Model, ModelObjects, Position, ModelObject, KnownObjects, ExtractExtended, ExtractType } from '@/model'
import { MonsterType, TreasureType, PlaceType, Trolligion, TrollRace, Troll, Place, Treasure, Monster, PlaceProperties, TrollProperties } from '@/model/mhtypes'
import { MHEvents, MHEvent } from '@/model/events'
import { SCIZEvent, mergeSCIZRequests } from '@/model/sciz'
import { Mission, MZResponseObject, MZMonster, MZResponse, readMissions } from '@/model/mountyzilla'
import { BricollResponse } from '@/model/bricoltrolls'
import { bricolList, parseMayBeNumber } from './utils'
import { parse } from 'date-fns'
import { Md5 } from 'ts-md5'

interface KnownColumns {
  name: string | number
  type: string | number
}

interface TableParseOptions<T extends KnownObjects> {
  columns?: Partial<KnownColumns>;
  type: ExtractType<T>;
  additionalInfoParser?: (element: Element, type: ExtractType<T>) => ExtractExtended<T>;
}

const DEFAULT_TABLE_COLUMNS = {
  name: 3,
  type: 3
}

type ColumnDefinition<Names extends string> = {
  [Name in Names]: number | string
}

type ColumnIndices<Names extends string> = {
  [Name in Names]: number
}

function extractColumnIndex<T extends string> (tableNode: HTMLElement | null, columns: ColumnDefinition<T>): ColumnIndices<T> {
  const finder = tableNode === null ? [] : Array.from(tableNode.querySelectorAll('thead > tr > *')).map((th, index): [Element, number] => [th, index])

  const columnIndices: Partial<ColumnIndices<T>> = {}

  for (const [columnName, currentValue] of (Object.entries(columns) as [T, ColumnDefinition<T>[T]][])) {
    let newIndex = -1
    if (typeof currentValue === 'number') {
      newIndex = currentValue
    } else {
      for (const [header, index] of finder) {
        if (header.textContent === currentValue) {
          newIndex = index
          break
        }
      }
    }
    columnIndices[columnName] = newIndex
  }

  return columnIndices as ColumnIndices<T>
}

function tdSelector<T extends string> (indices: ColumnIndices<T>, name: T) {
  return `td:nth-child(${indices[name] + 1})`
}

function extractTrText (tr: Element, trSelector: string) {
  return tr.querySelector(trSelector)?.textContent?.trim()
}

function extractCellText<T extends string> (tr: Element, columnIndices: ColumnIndices<T>, columnName: T) {
  return extractTrText(tr, tdSelector(columnIndices, columnName))
}

function parseTable<T extends KnownObjects> (tableNode: HTMLElement | null,
  parseOptions: TableParseOptions<T>
): ModelObjects<T> {
  if (tableNode === null) {
    return new ModelObjects()
  }

  const columnIndices = extractColumnIndex(tableNode, {
    ...DEFAULT_TABLE_COLUMNS,
    ...parseOptions?.columns || {}
  })

  const trs = tableNode.querySelectorAll('tbody > tr')
  const elements = Array.from(trs).map(tr => {
    const name = extractCellText(tr, columnIndices, 'name')
    const typeSelector = tdSelector(columnIndices, 'type')
    const typeText = extractTrText(tr, typeSelector)
    const type = parseOptions.type.switch(typeText, tr.querySelector(typeSelector))
    return new ModelObject<T>(
      extractTrText(tr, 'td:nth-child(3)'),
      name,
      type,
      new Position(
        extractTrText(tr, 'td:nth-last-child(3)'),
        extractTrText(tr, 'td:nth-last-child(2)'),
        extractTrText(tr, 'td:nth-last-child(1)')
      ),
      extractTrText(tr, 'td:nth-child(1)'),
      parseOptions?.additionalInfoParser ? parseOptions.additionalInfoParser(tr, type) : ({} as ExtractExtended<T>)
    )
  }).filter(({ id, name }) => name !== '' && !Number.isNaN(id))

  return new ModelObjects<T>(elements)
}

function matchingMissions (type: MonsterType, missions: Mission[]): Mission[] {
  return missions.filter(mission => {
    if (mission.type === 'Race' && type.name === mission.race) {
      return true
    } else if (mission.type === 'Famille' && type.features.familyName === mission.famille) {
      return true
    }
    return false
  })
}

function parseMonsters (missions: Mission[]) {
  const tableNode = document.getElementById('VueMONSTRE')
  const columnIndices = extractColumnIndex(tableNode, {
    name: 'Nom'
  })
  const monsters = parseTable<Monster>(document.getElementById('VueMONSTRE'),
    {
      columns: { name: 'Nom', type: 'Nom' },
      type: MonsterType.UNKNOWN,
      additionalInfoParser: (tr: Element, type: MonsterType) => {
        const text = extractCellText(tr, columnIndices, 'name') ?? ''
        return {
          mz: null,
          events: new MHEvents(),
          missions: matchingMissions(type, missions),
          ...type.extractFeatures(text)
        }
      }
    })
  return monsters
}

function parseTreasures () {
  const treasures = parseTable<Treasure>(document.getElementById('VueTRESOR'), { type: TreasureType.UNKNOWN })
  return treasures
}

function parsePlaces () {
  const places = parseTable<Place>(
    document.getElementById('VueLIEU'),
    {
      type: PlaceType.UNKNOWN,
      additionalInfoParser: () => ({
        owner: null,
        services: []
      })
    })
  return places
}

function parseTrolls () {
  const tableNode = document.getElementById('VueTROLL')
  const columnIndices = extractColumnIndex(tableNode, {
    guild: 'Guilde',
    level: 'Niv.',
    name: 'Nom'
  })

  const trollParser = (tr: Element) => {
    const nameTd = tr.querySelector(tdSelector(columnIndices, 'name'))
    const hidden = nameTd?.querySelector('img[title="Caché"]') !== null ?? false
    const tags = Array.from(nameTd?.querySelectorAll('img:not([title="Caché"])') ?? []).map(element => {
      if (element instanceof HTMLImageElement) {
        return {
          image: element.src,
          title: element.title
        }
      }
      return undefined
    }).filter(e => e !== undefined) as TrollProperties['tags']
    return {
      guild: extractCellText(tr, columnIndices, 'guild') ?? null,
      level: parseMayBeNumber(extractCellText(tr, columnIndices, 'level')),
      trolligion: Trolligion.UNKNOWN.switch((tr.querySelector(`${tdSelector(columnIndices, 'name')} > img:last-child`) as HTMLImageElement)?.title),
      sciz: null,
      hidden,
      invisible: null,
      camouflaged: null,
      events: new MHEvents(),
      tags
    }
  }
  const trolls = parseTable<Troll>(
    tableNode,
    {
      columns: { type: 'Race', name: 'Nom' },
      type: TrollRace.UNKNOWN,
      additionalInfoParser: trollParser
    })
  return trolls
}

function parsePosition (): Position {
  const rawPosition = document.body.querySelector('form[name=LimitViewForm] > table > tbody > tr:first-child > td:first-child > ul > li:first-child > b:last-child')
  const [, x, y, n] = /X = (-?\d+),\s*Y = (-?\d+),\s*N =\s*(-?\d+)/m.exec(rawPosition?.textContent ?? '') ?? ['', '', '', '']
  return new Position(x, y, n)
}

async function scizTrolls (model: Model) {
  const trollIds = Array.from(Object.keys(model.trolls))
  const data = await mergeSCIZRequests('https://www.sciz.fr/api/hook/trolls', {
    method: 'POST',
    body: JSON.stringify({ ids: trollIds })
  })

  data.trolls?.forEach(scizTroll => {
    const myTroll = model.trolls[scizTroll.id]
    if (myTroll === undefined) {
      return
    }
    myTroll.extended.sciz = scizTroll
  })
}

async function scizTreasures (model: Model) {
  const treasureIds = Array.from(Object.keys(model.treasures)).slice(0, 100)
  const data = await mergeSCIZRequests('https://www.sciz.fr/api/hook/treasures', {
    method: 'POST',
    body: JSON.stringify({ ids: treasureIds })
  })

  data.treasures?.forEach(scizTreasure => {
    const myTreasure = model.treasures[scizTreasure.id]
    if (myTreasure === undefined) {
      return
    }
    myTreasure.name = scizTreasure.nom
    myTreasure.extended.sciz = scizTreasure
  })
}

async function readSCIZ (model: Model) {
  await Promise.all([
    scizTrolls(model),
    scizTreasures(model)
  ])
}

function isMZMonster (element: MZResponseObject): element is MZMonster {
  return typeof (element as MZMonster).id === 'number'
}

async function readMZ (model: Model) {
  const monsterIds = Object.values(model.monsters).map(monster => ({
    id: monster.id,
    nom: monster.name
  }))
  const response = await fetch('//mz.mh.raistlin.fr/mz/getCaracMonstre.php', {
    method: 'POST',
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    },
    body: 'l=' + JSON.stringify(monsterIds)
  })

  const data = await response.json() as MZResponse
  data.forEach(mzMonster => {
    if (!isMZMonster(mzMonster)) {
      return
    }
    const myMonster = model.monsters[mzMonster.id]
    if (myMonster === undefined) {
      return
    }

    myMonster.extended.mz = mzMonster
  })
}

async function readBricolTrolls (model: Model) {
  for (const options of await bricolList()) {
    const response = await fetch(`//it.mh.raistlin.fr/${options.name}/mz_json.php?login=${options.login}&password=${Md5.hashStr(options.password)}`)
    const data = await response.json() as BricollResponse
    const subModel = new Model()
    for (const troll of Object.values(data.data.trolls).filter(troll => troll.id !== model.me)) {
      if (!(troll.id in model.trolls)) {
        const position = new Position(troll.x, troll.y, troll.n)
        subModel.trolls[troll.id] = new ModelObject<Troll>(
          troll.id,
          troll.nom,
          TrollRace.UNKNOWN.switch(troll.race),
          position,
          position.distance(model.position),
          {
            guild: null,
            level: troll.niveau,
            trolligion: Trolligion.UNKNOWN,
            sciz: null,
            hidden: troll.camoufle || troll.invisible,
            camouflaged: troll.camoufle,
            invisible: troll.invisible,
            events: new MHEvents(),
            tags: []
          }
        )
      }
    }
    await scizTrolls(subModel)
    model.trolls = new ModelObjects<Troll>([...Object.values(model.trolls), ...Object.values(subModel.trolls)])
  }
}

function readPage (model: Model) {
  model.me = parseMayBeNumber((document.querySelector('input[name="ai_IdPJ"]') as HTMLInputElement)?.value)
  model.position = parsePosition()
  model.monsters = parseMonsters(Object.values(readMissions(model.me)))
  model.treasures = parseTreasures()
  model.places = parsePlaces()
  model.trolls = parseTrolls()
}

export async function buildModel (model: Model): Promise<Model> {
  readPage(model)
  model.loading = false
  await Promise.all([readSCIZ(model), readMZ(model), readBricolTrolls(model)])
  return model
}

export function parseEvents (table: HTMLTableElement | null): MHEvent[] {
  const result: MHEvent[] = []
  if (table === null) {
    return result
  }
  table.querySelectorAll('tbody > tr').forEach((tr: Element) => {
    const [date, type, description] = Array.from(tr.children).map(element => (element as HTMLElement).innerText)
    result.push(new MHEvent(parse(date, 'dd/MM/yyyy HH:mm:ss', new Date()), type, description.trim()))
  })
  return result
}

async function loadHTML (uri: string) {
  const response = await fetch(uri)
  const charset = ((response.headers.get('content-type') ?? '').match(/charset=(.*)/) ?? ['', ''])[1]
  const data = charset ? new TextDecoder(charset).decode(await response.arrayBuffer()) : await response.text()
  return new DOMParser().parseFromString(data, 'text/html')
}

export async function loadEvents (uri: string, selector: string): Promise<MHEvent[]> {
  const document = await loadHTML(uri)
  return parseEvents(document.querySelector(selector))
}

export async function loadSCIZEvents (id: number, begin: Date, end: Date): Promise<SCIZEvent[]> {
  const response = await mergeSCIZRequests(`https://www.sciz.fr/api/hook/events/${id}/${begin.getTime() - MHEvents.EVENT_TIME_TOLERANCE}/${end.getTime() + MHEvents.EVENT_TIME_TOLERANCE}`)
  return response.events ?? []
}

export async function loadServices (place: ModelObject<Place>): Promise<PlaceProperties> {
  const document = await loadHTML(`/mountyhall/View/TaniereDescription.php?ai_IDLieu=${place.id}`)

  const [ownerDiv, servicesDiv] = Array.from(document.querySelectorAll('table tr > td > div')).slice(1, 3)
  const owner = ownerDiv?.querySelector('a')?.textContent?.trim() ?? null
  const services = servicesDiv?.textContent?.split('|').slice(1).map(name => name.trim()).filter(name => name) ?? []

  return { owner, services }
}
