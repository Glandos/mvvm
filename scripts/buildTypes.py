import re
import sys
import unicodedata

from io import StringIO
from ftplib import FTP

SPURIOUS_UNDERSCORES = re.compile('(?:(?<=_)_|_+$)')

def makeVariableName(name):
    return SPURIOUS_UNDERSCORES.sub("", "".join(unicodedata.normalize("NFKD", c)[0] for c in name).translate(str.maketrans("- '()", "_____")).upper())

ftp = FTP('ftp.mountyhall.com')
ftp.login()

def printMonster(line):
    [id, name, gender, uri, _] = line.strip().split(";", 5)
    uri = '' if uri == '-' else f", '{uri}'"
    variableName = makeVariableName(name)
    name = name.replace("'", "\\'")
    print(f"static {variableName} = new MonsterType('{name}'{uri})")


ftp.retrlines('RETR Public_Monstres.txt',  callback=printMonster)

treasures = {}

def printTreasure(line):
    [id, name, type, _] = line.strip().split(";", 4)
    treasures.setdefault(type, []).append(name)

ftp.retrlines('RETR Public_Tresors.txt',  callback=printTreasure)

for (type, names) in treasures.items():
    variableName = makeVariableName(type)
    #names = [name.replace("'", "\\'") for name in names]
    print(f"static {variableName} = new TreasureType('{type}', {names})")