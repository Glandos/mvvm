// eslint-disable-next-line @typescript-eslint/no-var-requires
const WebpackUserscript = require('webpack-userscript')

const production = process.env.NODE_ENV === 'production'

module.exports = {
  css: {
    extract: false
  },
  configureWebpack: {
    plugins: [
      new WebpackUserscript({
        metajs: false,
        downloadBaseUrl: production ? 'https://adrien.antipoul.fr/mvvm/' : 'http://localhost:8080/',
        headers: {
          namespace: 'MH',
          match: 'https://games.mountyhall.com/mountyhall/MH_Play/Play_vue.php',
          grant: ['GM.getValue', 'GM.setValue'],
          version: '[buildTime]',
          'run-at': 'document-end',
          description: 'Récupère toutes les informations de la page, et en fait un joli modèle'
        }
      })
    ]
  }
}
